package com.sinanbir.todos.listener;

public interface ItemClickListener {
    void onItemClicked(long todoId);
}
