package com.sinanbir.todos.adapter;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sinanbir.todos.R;
import com.sinanbir.todos.database.Todo;
import com.sinanbir.todos.database.TodoPriority;
import com.sinanbir.todos.listener.ItemClickListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("unchecked") public class TodoAdapter extends RealmRecyclerViewAdapter {

    private RealmResults<Todo> todos;
    private ItemClickListener itemClickListener;
    private String emptyText;
    private SimpleDateFormat dateFormat;
    private final int VIEW_TYPE_EMPTY = 0;
    private final int VIEW_TYPE_TODO = 1;
    @ColorInt private int red300, green300, yellow300;

    public TodoAdapter(RealmResults<Todo> todos, ItemClickListener itemClickListener,
        String emptyString, Context context) {
        super(todos, true, true);
        this.todos = todos;
        this.itemClickListener = itemClickListener;
        this.emptyText = emptyString;
        dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
        red300 = ContextCompat.getColor(context, R.color.red_300);
        green300 = ContextCompat.getColor(context, R.color.green_300);
        yellow300 = ContextCompat.getColor(context, R.color.yellow_300);
    }

    private class TodoViewHolder extends RecyclerView.ViewHolder {
        TextView content, expireDate;
        CardView container;

        TodoViewHolder(View view) {
            super(view);
            container = view.findViewById(R.id.row_todo_cardview);
            content = view.findViewById(R.id.row_todo_content);
            expireDate = view.findViewById(R.id.row_todo_expire_date);
        }
    }

    private class EmptyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        EmptyViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.textview_todo_empty);
        }
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            View row = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_todo_empty, parent, false);
            return new EmptyViewHolder(row);
        } else if (viewType == VIEW_TYPE_TODO) {
            View row =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.row_todo, parent, false);
            return new TodoViewHolder(row);
        }
        return null;
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TodoViewHolder) {
            TodoViewHolder todoViewHolder = (TodoViewHolder) holder;
            int adapterPosition = holder.getAdapterPosition();
            Todo todo = todos.get(adapterPosition);
            Date date = new Date(todo.getExpireTime());
            String dateString = dateFormat.format(date);

            switch (todo.getPriority()) {
                case TodoPriority.LOW:
                    todoViewHolder.container.setCardBackgroundColor(yellow300);
                    break;
                case TodoPriority.HIGH:
                    todoViewHolder.container.setCardBackgroundColor(red300);
                    break;
                case TodoPriority.NORMAL:
                default:
                    todoViewHolder.container.setCardBackgroundColor(green300);
                    break;
            }

            todoViewHolder.content.setText(todo.getContent());
            todoViewHolder.expireDate.setText(dateString);

            todoViewHolder.container.setOnClickListener(
                __ -> itemClickListener.onItemClicked(todo.getId()));
        } else if (holder instanceof EmptyViewHolder) {
            EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
            emptyViewHolder.textView.setText(emptyText);
        }
    }

    @Override public int getItemCount() {
        if (todos.size() == 0) {
            return 1;
        } else {
            return todos.size();
        }
    }

    @Override public int getItemViewType(int position) {
        if (todos.size() == 0) {
            return VIEW_TYPE_EMPTY;
        } else {
            return VIEW_TYPE_TODO;
        }
    }
}
