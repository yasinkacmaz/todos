package com.sinanbir.todos.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sinanbir.todos.main.ExpiredFragment;
import com.sinanbir.todos.main.UpcomingFragment;

public class TabPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence[] titles;

    public TabPagerAdapter(FragmentManager fm, CharSequence[] titles) {
        super(fm);
        this.titles = titles;
    }

    @Override public Fragment getItem(int position) {
        if (position == 0) {
            return new UpcomingFragment();
        } else {
            return new ExpiredFragment();
        }
    }

    @Override public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override public int getCount() {
        return titles.length;
    }
}
