package com.sinanbir.todos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import io.realm.Realm;

public class BaseActivity extends AppCompatActivity {
    public AppCompatActivity thisActivity = this;
    public Context context;
    Realm realm;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        realm = Realm.getDefaultInstance();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void showShortToast(@StringRes int resId) {
        Toast.makeText(context, getString(resId), Toast.LENGTH_SHORT).show();
    }

    public Realm getRealm() {
        return realm;
    }
}
