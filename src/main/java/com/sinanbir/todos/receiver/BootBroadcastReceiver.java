package com.sinanbir.todos.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.sinanbir.todos.database.Todo;
import com.sinanbir.todos.utils.AlarmUtils;
import io.realm.Realm;
import io.realm.RealmResults;

public class BootBroadcastReceiver extends BroadcastReceiver {
    private Realm realm;

    @Override public void onReceive(Context context, Intent intent) {
        realm = Realm.getDefaultInstance();
        handleAlarms(context);
    }

    private void handleAlarms(Context context) {
        RealmResults<Todo> todos = realm.where(Todo.class).findAll();
        for (Todo todo : todos) {
            AlarmUtils.setAlarm(context, todo.getId(), todo.getExpireTime());
        }
        realm.close();
    }
}
