package com.sinanbir.todos.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.sinanbir.todos.R;
import com.sinanbir.todos.TodoActivity;
import com.sinanbir.todos.database.Todo;
import com.sinanbir.todos.utils.NotificationUtils;
import com.sinanbir.todos.utils.Utils;
import io.realm.Realm;

public class NotificationReceiver extends BroadcastReceiver {
    public static String TODO_ID = "notificationId";
    public static String PENDING_INTENT_REQUEST_CODE = "pending_intent_request_code";

    @Override public void onReceive(Context context, Intent intent) {
        Realm realm = Realm.getDefaultInstance();
        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Bundle extras = intent.getExtras();
        if (extras != null) {

            long todoId = extras.getLong(TODO_ID);
            int piRequestCode = extras.getInt(PENDING_INTENT_REQUEST_CODE);

            Todo todo = realm.where(Todo.class).equalTo(Todo.ID, todoId).findFirst();
            if (todo != null) {
                Intent todoIntent = TodoActivity.newIntent(context, todoId);
                todoIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent piTodo = PendingIntent.getActivity(context, piRequestCode, todoIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder notificationCompat;

                notificationCompat = new NotificationCompat.Builder(context,
                    NotificationUtils.getChannelIdByPriority(todo.getPriority())).setSmallIcon(
                    R.drawable.ic_notification)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setAutoCancel(true)
                    .setColor(Utils.generateColorForPriority(todo.getPriority(), context))
                    .setContentText(todo.getContent())
                    .setContentIntent(piTodo)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(todo.getContent()));

                if (notificationManager != null) {
                    notificationManager.notify((int) System.currentTimeMillis(),
                        notificationCompat.build());
                }

                notificationCompat.build().flags |= NotificationCompat.FLAG_AUTO_CANCEL;
            }
        }
    }
}
