package com.sinanbir.todos.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import com.sinanbir.todos.database.TodoPriority;

public class NotificationUtils extends ContextWrapper {

    private NotificationManager mManager;
    public static final String HIGH_CHANNEL_ID = "com.sinanbir.todos.HIGH";
    public static final String NORMAL_CHANNEL_ID = "com.sinanbir.todos.NORMAL";
    public static final String LOW_CHANNEL_ID = "com.sinanbir.todos.LOW";
    public static final String HIGH_CHANNEL_NAME = "HIGH CHANNEL";
    public static final String NORMAL_CHANNEL_NAME = "NORMAL CHANNEL";
    public static final String LOW_CHANNEL_NAME = "LOW CHANNEL";

    public NotificationUtils(Context base) {
        super(base);
        if (Utils.isAtLeastOreo()) createChannels();
    }

    @RequiresApi(Build.VERSION_CODES.O) public void createChannels() {
        NotificationChannel highChannel =
            new NotificationChannel(HIGH_CHANNEL_ID, HIGH_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH);
        highChannel.enableLights(true);
        highChannel.enableVibration(true);
        highChannel.setLightColor(Color.RED);
        highChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(highChannel);

        NotificationChannel normalChannel =
            new NotificationChannel(NORMAL_CHANNEL_ID, NORMAL_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT);
        normalChannel.enableLights(true);
        normalChannel.enableVibration(true);
        normalChannel.setLightColor(Color.GREEN);
        normalChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(normalChannel);

        NotificationChannel lowChannel = new NotificationChannel(LOW_CHANNEL_ID, LOW_CHANNEL_NAME,
            NotificationManager.IMPORTANCE_LOW);
        lowChannel.enableLights(true);
        lowChannel.setLightColor(Color.YELLOW);
        lowChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(lowChannel);
    }

    public static String getChannelIdByPriority(@TodoPriority int todoPriority) {
        switch (todoPriority) {
            case TodoPriority.HIGH:
                return HIGH_CHANNEL_ID;
            case TodoPriority.LOW:
                return LOW_CHANNEL_ID;
            case TodoPriority.NORMAL:
                return NORMAL_CHANNEL_ID;
            default:
                return NORMAL_CHANNEL_ID;
        }
    }

    private NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }
}