package com.sinanbir.todos.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.sinanbir.todos.receiver.NotificationReceiver;

public class AlarmUtils {

    public static void setAlarm(Context context, long todoId, long expireDate) {
        Intent alarmIntent = new Intent(context, NotificationReceiver.class);
        alarmIntent.putExtra(NotificationReceiver.TODO_ID, todoId);
        alarmIntent.putExtra(NotificationReceiver.PENDING_INTENT_REQUEST_CODE, todoId);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) todoId, alarmIntent,
            PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, expireDate, pendingIntent);
        }
    }
}
