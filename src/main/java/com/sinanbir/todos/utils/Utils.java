package com.sinanbir.todos.utils;

import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import com.sinanbir.todos.R;
import com.sinanbir.todos.database.TodoPriority;

public class Utils {
    public static boolean isAtLeastOreo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static @ColorInt int generateColorForPriority(@TodoPriority int todoPriority,
        Context context) {
        switch (todoPriority) {
            case TodoPriority.HIGH:
                return ContextCompat.getColor(context, R.color.red_500);
            case TodoPriority.LOW:
                return ContextCompat.getColor(context, R.color.yellow_500);
            case TodoPriority.NORMAL:
            default:
                return ContextCompat.getColor(context, R.color.green_500);
        }
    }
}
