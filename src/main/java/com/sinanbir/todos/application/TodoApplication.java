package com.sinanbir.todos.application;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TodoApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("todos.realm")
            .deleteRealmIfMigrationNeeded()
            .build();

        Realm.setDefaultConfiguration(config);
    }
}
