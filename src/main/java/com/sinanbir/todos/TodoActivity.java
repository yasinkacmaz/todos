package com.sinanbir.todos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.sinanbir.todos.database.DatabaseManager;
import com.sinanbir.todos.database.Todo;
import com.sinanbir.todos.database.TodoPriority;
import com.sinanbir.todos.utils.AlarmUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TodoActivity extends BaseActivity {
    Toolbar toolbar;
    EditText editTextContent;
    RelativeLayout dateLayout, timeLayout;
    Spinner prioritySpinner;
    TextView textViewDate, textViewTime;
    private final static String INTENT_KEY_ID = "todo_id";
    boolean isEditing = false;
    long todoId = -1;
    @TodoPriority private int todoPriority = TodoPriority.NORMAL;
    Todo existingTodo;
    Calendar calendar;
    SimpleDateFormat dateFormat;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        initFields();
        initViews();
        getIntentExtras();
        handleShareIntent(getIntent());
    }

    private void initFields() {
        calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        textViewDate = findViewById(R.id.create_todo_date_textview);
        textViewTime = findViewById(R.id.create_todo_time_textview);
        editTextContent = findViewById(R.id.create_todo_content);
        dateLayout = findViewById(R.id.create_todo_date_layout);
        dateLayout.setOnClickListener(__ -> showDatePicker());
        timeLayout = findViewById(R.id.create_todo_time_layout);
        timeLayout.setOnClickListener(__ -> showTimePicker());

        setTimeText();
        setDateText();

        prioritySpinner = findViewById(R.id.create_todo_priority_spinner);

        ArrayAdapter<CharSequence> adapter =
            ArrayAdapter.createFromResource(this, R.array.priorities,
                android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(adapter);

        prioritySpinner.setSelection(todoPriority);
        prioritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                todoPriority = i;
            }

            @Override public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void showDatePicker() {
        Calendar calendarNow = Calendar.getInstance();
        calendarNow.setTimeInMillis(System.currentTimeMillis());
        DatePickerDialog datePickerDialog =
            new DatePickerDialog(thisActivity, (view, year, monthOfYear, dayOfMonth) -> {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                setDateText();
            }, calendarNow.get(Calendar.YEAR), calendarNow.get(Calendar.MONTH),
                calendarNow.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendarNow.getTimeInMillis());
        datePickerDialog.show();
    }

    private void showTimePicker() {
        Calendar calendarNow = Calendar.getInstance();
        calendarNow.setTimeInMillis(System.currentTimeMillis());
        int hour = calendarNow.get(Calendar.HOUR_OF_DAY);
        int minute = calendarNow.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog =
            new TimePickerDialog(thisActivity, (timePicker, selectedHour, selectedMinute) -> {
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                calendar.set(Calendar.MINUTE, selectedMinute);
                setTimeText();
            }, hour, minute, true);
        timePickerDialog.show();
    }

    private void setCalendar(long time) {
        calendar.setTimeInMillis(time);
        setDateText();
        setTimeText();
    }

    private void setDateText() {
        String datetime = dateFormat.format(calendar.getTime());
        textViewDate.setText(datetime);
    }

    private void setTimeText() {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        textViewTime.setText(String.format(Locale.getDefault(), "%02d:%02d", hour, minutes));
    }

    private void getIntentExtras() {
        if (getIntent().hasExtra(INTENT_KEY_ID)) {
            isEditing = true;
            todoId = getIntent().getLongExtra(INTENT_KEY_ID, -1);
            getExistingTodo();
            setTitle(R.string.title_activity_todo_edit);
        }
    }

    private void getExistingTodo() {
        existingTodo = DatabaseManager.getInstance().getTodo(realm, todoId);
        todoPriority = existingTodo.getPriority();
        prioritySpinner.setSelection(todoPriority);
        editTextContent.setText(existingTodo.getContent());
        setCalendar(existingTodo.getExpireTime());
    }

    @Override public void onBackPressed() {
        String content = editTextContent.getText().toString();
        if (isEditing) {
            boolean isChanged = false;
            if (todoPriority != existingTodo.getPriority()) isChanged = true;
            if (!content.equals(existingTodo.getContent())) isChanged = true;
            if (calendar.getTimeInMillis() != existingTodo.getExpireTime()) isChanged = true;
            if (isChanged) {
                DatabaseManager.getInstance()
                    .updateTodo(realm, content, calendar.getTimeInMillis(), todoId, todoPriority);
            }
            navigateBack();
        } else {
            if (todoId == -1 && !content.isEmpty()) {
                long todoId = System.currentTimeMillis();
                AlarmUtils.setAlarm(getApplicationContext(), todoId, calendar.getTimeInMillis());
                DatabaseManager.getInstance()
                    .addNewTodo(realm, content, calendar.getTimeInMillis(), todoId, todoPriority);
                navigateBack();
            } else if (todoId != -1 && content.isEmpty()) {
                showShortToast(R.string.please_add_todo_content);
            } else {
                navigateBack();
            }
        }
    }

    private void navigateBack() {
        super.onBackPressed();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        if (isEditing) getMenuInflater().inflate(R.menu.menu_todo, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_share:
                shareTodo();
                return true;
            case R.id.action_delete:
                deleteTodo();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareTodo() {
        String shareString = getString(R.string.todo_share, existingTodo.getContent());
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareString);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void deleteTodo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.delete_todo);
        builder.setMessage(R.string.todo_delete_information);
        builder.setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
            DatabaseManager.getInstance().deleteTodo(realm, todoId);
            navigateBack();
        });

        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    public void handleShareIntent(Intent intent) {
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.equals("text/plain")) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null) {
                    if (!editTextContent.getText().toString().isEmpty()) {
                        editTextContent.append("\n");
                    }
                    editTextContent.append(sharedText);
                }
            }
        }
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleShareIntent(intent);
    }

    public static Intent newIntent(Context context, long todoId) {

        Intent todoIntent = new Intent(context, TodoActivity.class);

        todoIntent.putExtra(INTENT_KEY_ID, todoId);
        return todoIntent;
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, TodoActivity.class);
    }
}
