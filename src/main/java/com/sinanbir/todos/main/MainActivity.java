package com.sinanbir.todos.main;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import com.sinanbir.todos.BaseActivity;
import com.sinanbir.todos.R;
import com.sinanbir.todos.TodoActivity;
import com.sinanbir.todos.adapter.TabPagerAdapter;
import com.sinanbir.todos.listener.ItemClickListener;

@SuppressWarnings("FieldCanBeLocal") public class MainActivity extends BaseActivity
    implements ItemClickListener {

    private Toolbar toolbar;
    private FloatingActionButton fab;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabPagerAdapter tabPagerAdapter;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            startActivity(TodoActivity.newIntent(context));
            overridePendingTransition(R.anim.slide_in_right, R.anim.nothing);
        });

        CharSequence[] titles = getResources().getStringArray(R.array.tab_titles);
        tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), titles);

        viewPager = findViewById(R.id.main_activity_view_pager);
        viewPager.setAdapter(tabPagerAdapter);

        tabLayout = findViewById(R.id.main_activity_tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override public void onItemClicked(long todoId) {
        startActivity(TodoActivity.newIntent(context, todoId));
        overridePendingTransition(R.anim.slide_in_right, R.anim.nothing);
    }

    @Override public void onBackPressed() {
        moveTaskToBack(true);
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
}
