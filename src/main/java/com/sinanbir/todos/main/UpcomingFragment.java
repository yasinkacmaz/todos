package com.sinanbir.todos.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sinanbir.todos.R;
import com.sinanbir.todos.WrapContentLLM;
import com.sinanbir.todos.adapter.TodoAdapter;
import com.sinanbir.todos.database.DatabaseManager;
import com.sinanbir.todos.database.Todo;

import io.realm.RealmResults;

@SuppressWarnings("FieldCanBeLocal") public class UpcomingFragment extends Fragment {

    private View rootView;
    private RecyclerView todosRecyclerView;
    private WrapContentLLM linearLayoutManager;
    private TodoAdapter todoAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.upcoming_fragment, container, false);

        initRecyclerView();
        return rootView;
    }

    private void initRecyclerView() {
        todosRecyclerView = rootView.findViewById(R.id.upcoming_fragment_recyclerview_todos);
        linearLayoutManager = new WrapContentLLM(getContext(), LinearLayoutManager.VERTICAL, false);
        todosRecyclerView.setLayoutManager(linearLayoutManager);

        RealmResults<Todo> todos =
            DatabaseManager.getInstance().getUpcomingTodos(getParentActivity().getRealm());
        todoAdapter = new TodoAdapter(todos, getParentActivity(),
            getString(R.string.do_not_have_upcoming_todos), getContext());
        todosRecyclerView.setAdapter(todoAdapter);
    }

    private MainActivity getParentActivity() {
        return (MainActivity) getActivity();
    }
}
