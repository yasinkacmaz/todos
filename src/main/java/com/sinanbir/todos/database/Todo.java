package com.sinanbir.todos.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Todo extends RealmObject {

    public static final String EXPIRE_TIME = "expireTime";
    public static final String ID = "id";
    public static final String PRIORITY = "priority";

    @PrimaryKey private long id;
    private String content;
    private long expireTime;
    private @TodoPriority int priority;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public @TodoPriority int getPriority() {
        return priority;
    }

    public void setPriority(@TodoPriority int priority) {
        this.priority = priority;
    }
}
