package com.sinanbir.todos.database;

import android.support.annotation.IntDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({TodoPriority.LOW, TodoPriority.NORMAL, TodoPriority.HIGH})
@Retention(RetentionPolicy.SOURCE)
public @interface TodoPriority {
    int LOW = 0;
    int NORMAL = 1;
    int HIGH = 2;
}
