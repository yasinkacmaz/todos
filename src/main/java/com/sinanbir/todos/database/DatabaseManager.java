package com.sinanbir.todos.database;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.sinanbir.todos.database.Todo.EXPIRE_TIME;
import static com.sinanbir.todos.database.Todo.ID;
import static com.sinanbir.todos.database.Todo.PRIORITY;

public class DatabaseManager {

    private static DatabaseManager INSTANCE;

    public static synchronized DatabaseManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseManager();
        }
        return INSTANCE;
    }

    public RealmResults<Todo> getExpiredTodos(Realm realm) {
        Calendar calendar = Calendar.getInstance();
        return realm.where(Todo.class)
            .lessThanOrEqualTo(EXPIRE_TIME, calendar.getTimeInMillis())
            .findAllSorted(new String[] { PRIORITY, EXPIRE_TIME },
                new Sort[] { Sort.DESCENDING, Sort.ASCENDING });
    }

    public RealmResults<Todo> getUpcomingTodos(Realm realm) {
        Calendar calendar = Calendar.getInstance();
        return realm.where(Todo.class)
            .greaterThan(EXPIRE_TIME, calendar.getTimeInMillis())
            .findAllSorted(new String[] { PRIORITY, EXPIRE_TIME },
                new Sort[] { Sort.DESCENDING, Sort.ASCENDING });
    }

    public Todo getTodo(Realm realm, long todoId) {
        return realm.where(Todo.class).equalTo(ID, todoId).findFirst();
    }

    public void updateTodo(Realm realm, String content, long expireTime, long todoId,
        @TodoPriority int priority) {
        Todo todo = realm.where(Todo.class).equalTo(ID, todoId).findFirst();
        realm.beginTransaction();
        todo.setContent(content);
        todo.setExpireTime(expireTime);
        todo.setPriority(priority);
        realm.commitTransaction();
    }

    public void addNewTodo(Realm realm, String content, long expireTime, long todoId,
        @TodoPriority int priority) {
        realm.beginTransaction();
        Todo todo = realm.createObject(Todo.class, todoId);
        todo.setContent(content);
        todo.setExpireTime(expireTime);
        todo.setPriority(priority);
        realm.copyToRealmOrUpdate(todo);
        realm.commitTransaction();
    }

    public void deleteTodo(Realm realm, long todoId) {
        realm.beginTransaction();
        realm.where(Todo.class).equalTo("id", todoId).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }
}
